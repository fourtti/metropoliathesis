//pragma solidity ^0.4.24;
pragma solidity ^0.5.2;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";

contract MetropoliaToken is ERC20 {

	string public name = "MetropoliaToken";
	string public symbol = "MPT";
	uint8 public decimals = 2;
	uint public INITIAL_SUPPLY = 25000;
	
	constructor() public {
	  _mint(msg.sender, INITIAL_SUPPLY);
	}
}


