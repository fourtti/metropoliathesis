import MetropoliaToken from '../build/contracts/MetropoliaToken.json'
var contract = require("truffle-contract");

import HDWalletProvider from 'truffle-hdwallet-provider';

const Web3Library = require('web3');
const mnemonic = 'session world scan patient panic cloth wine morning update venture weasel humble'; // 12 word mnemonic
const Provider = new HDWalletProvider(mnemonic, "http://192.168.10.62:8545");
const web3 = new Web3Library(Provider);


class ContractService {

  constructor(props, context) {
    this.tokenContract = contract(MetropoliaToken)
  }

  //Get current Web3 account balance
  getBalance() {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(web3.currentProvider)
      web3.eth.getAccounts((error, accounts) => {
        this.tokenContract.deployed().then((instance) => {
          return instance.balanceOf(accounts[0]);
        }).then((result) => {
          let balance = result.c[0];
          resolve(balance)
        }).catch((error) => {
          console.error("Error" + error)
          reject(error)
        })
      })
    })
  }
}

const contractService = new ContractService()

export default contractService

/**
  // Get user info for account
  getUser(account) {
    return new Promise((resolve, reject) => {
      this.tokenContract.setProvider(web3.currentProvider)

      this.tokenContract.deployed().then((instance) => {
        instance.getUser.call(account)
        .then((user)  => {

          resolve(userObject)
        })
        .catch((error) => {
          console.log(`Error fetching account info for: ${account}`)
          reject(error)
        })
      })
    })
  }
}
**/