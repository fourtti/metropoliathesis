/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import './global';
import MetropoliaToken from './build/contracts/MetropoliaToken.json'
var contract = require("truffle-contract");


import HDWalletProvider from 'truffle-hdwallet-provider';
const Web3Library = require('web3');
const mnemonic = 'session world scan patient panic cloth wine morning update venture weasel humble'; // 12 word mnemonic
const Provider = new HDWalletProvider(mnemonic, "http://192.168.10.62:8545");
const web3 = new Web3Library(Provider);

import {Platform, StyleSheet, Text, View, Button} from 'react-native';

let tokenContract = contract(MetropoliaToken);
tokenContract.setProvider(Provider);

type Props = {};
export default class App extends Component<Props> {

    constructor() {
        super()

        this.state = {
            accountBalance: '...',
            accountAddress: '...',
            accountTokenBalance: '...'
        }
        this.buttonGetBalance = this.GetBalance.bind(this)
        this.buttonGetAccount = this.GetAccount.bind(this)
        this.buttonGetTokenBalance = this.GetTokenBalance.bind(this)
        this.buttonSendToken = this.SendTokens.bind(this)
    }

    //functions
    //Get account function
    GetAccount() {
        web3.eth.getAccounts().then((accounts) => {
             this.setState({accountAddress: accounts[0].toLowerCase()})
        });
    };

    //Get balance function
    GetBalance () {
        web3.eth.getAccounts().then((accounts) => {
            web3.eth.getBalance(accounts[0]).then((balance) => {
                this.setState({accountBalance: balance})
            });
        });
    };

    //Get token balance function
    GetTokenBalance () {
    return new Promise((resolve, reject) => {
          web3.eth.getAccounts((error, accounts) => {
            tokenContract.deployed().then((instance) => {
              return instance.balanceOf(accounts[0])
            }).then((result) => {
              let balance = result.words[0];
              resolve(balance);
              this.setState({accountTokenBalance: balance});
            }).catch((error) => {
              console.error("Error" + error);
              reject(error);
            });
          });
        });
    };

    //Send tokens function
    //This function will send 50 tokens to hardcoded address
    SendTokens() {
    return new Promise((resolve, reject) => {
          web3.eth.getAccounts((error, accounts) => {
            tokenContract.deployed().then((instance) => {
              instance.transfer("0x4e5Cd1658Fb713F46ae7fF1C8E919ca7120B566D", 50, {from: accounts[0]})
            }).catch((error) => {
              console.error("Error" + error);
              reject(error);
            })
          })
        })
    };


  render() {
    return (

      <View style={styles.container}>
        <Text style={styles.welcome}>ThesisApp</Text>
        <Button title={"get address"} onPress={this.buttonGetAccount}/>
        <Button title={"get balance"} onPress={this.buttonGetBalance}/>
        <Text>Account Address</Text>
        <Text>{this.state.accountAddress}</Text>
        <Text>Account balance</Text>
        <Text>{this.state.accountBalance}</Text>
        <Text></Text>
        <Button title={"get new token balance"} onPress={this.buttonGetTokenBalance}/>
        <Text>Metropolia Token amount</Text>
        <Text>{this.state.accountTokenBalance}</Text>
        <Button title={"send tokens to hardcoded address"} onPress={this.buttonSendToken}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
